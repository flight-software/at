#define _GNU_SOURCE
#define _DEFAULT_SOURCE

#include <sys/types.h>
#include <sys/wait.h>

#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "libdebug.h"
#include "libsll.h"

#include "at_db.h"
#include "at_job.h"

struct at_executor_arg {
    at_job* job;
    sqlite3* db;
};

struct at_pipe_names {
    int rd;
    int wr;
};

union at_pipe {
    struct at_pipe_names named;
    int array[2];
};

struct at_read_pipe_arg {
    size_t bytes_read;
    int fd;
};

static void setup_sighandler(void);
static at_job* wait_for_work(sll_fs* sorted_job_list, pthread_mutex_t* sll_lock);
static at_job* get_work_and_sleep(sqlite3* db, sll_fs* sorted_job_list, pthread_mutex_t* sll_lock);
static void* thread_read_pipe(void* x);
static void create_and_join_pipe_threads(int stdout_fd, int stderr_fd, char** stdout_buffer, char** stderr_buffer);
static const char* wait_for_job_status(pid_t child);
static void print_db(sqlite3* db);
static void* executor(void* x);
static void handler(int signum);

static int sleeper_running = 0;

void handler(int signum)
{
    if(signum == SIGUSR1) {
        P_INFO_STR("handling sigusr1");
    }
}

static void setup_sighandler(void)
{
    struct sigaction act;
    memset(&act, 0, sizeof(act));
    act.sa_handler = handler;
    sigaction(SIGUSR1, &act, NULL);
}

// lock will be held and work is available when this function returns
static at_job* wait_for_work(sll_fs* sorted_job_list, pthread_mutex_t* sll_lock)
{
    pthread_mutex_lock(sll_lock);
    at_job* job;
    while(!(job = sll_peek_head(sorted_job_list))) {
        pthread_mutex_unlock(sll_lock);
        pause();
        pthread_mutex_lock(sll_lock);
    }
    return job;
}

static at_job* get_work_and_sleep(sqlite3* db, sll_fs* sorted_job_list, pthread_mutex_t* sll_lock)
{
    while(1) {
        at_job* job = wait_for_work(sorted_job_list, sll_lock);

        time_t now = time(NULL);
        if(job->time < now) {
            P_ERR("time is too late, supposed to execute at %" PRId32 ", current time is %" PRId32, (int32_t)job->time,
                  (int32_t)now);
            if(insert_job_output_into_db(db, "", "", JOB_FAILED_TIME_PASSED, job->job_number)) {
                P_ERR("couldn't insert failed job %d into db", job->job_number);
            }
            sll_pull_head(sorted_job_list);
            free(job);

            pthread_mutex_unlock(sll_lock);
            continue;
        }
        // need to unlock here so that further jobs can be put into the sll
        pthread_mutex_unlock(sll_lock);

        P_INFO("Sleeping for %" PRId32, (((int32_t)job->time) - (int32_t)now));
        uint32_t sleep_time = (uint32_t)job->time - (uint32_t)now;

        uint32_t leftover_time = sleep(sleep_time);
        P_INFO("Slept for %" PRIu32 "/%" PRIu32, sleep_time - leftover_time, sleep_time);

        // sleep(3) is interrupted by the SIGUSR1
        // this means that a new job might be available as the head
        if(leftover_time > 0) {
            continue;
        }

        return job;
    }
    return NULL;
}

void* sleeper(void* x)
{
    if(sleeper_running) {
        P_ERR_STR("Sleeper thread already running");
        return (void*)EXIT_FAILURE;
    }

    sleeper_running = 1;

    at_sleeper_arg* sleeper_arg = x;

    setup_sighandler();

    while(1) {
        at_job* job = get_work_and_sleep(sleeper_arg->db, sleeper_arg->sorted_job_list, sleeper_arg->sll_lock);

        pthread_mutex_lock(sleeper_arg->sll_lock);

        // for the race condition between getting a job, then sleeping, and now going to execute
        if(job != sll_peek_head(sleeper_arg->sorted_job_list)) {
            pthread_mutex_unlock(sleeper_arg->sll_lock);
            continue;
        }

        sll_pull_head(sleeper_arg->sorted_job_list); // same as job

        // spawn child thread
        pthread_t id;
        struct at_executor_arg* executor_arg = malloc(sizeof(struct at_executor_arg));
        executor_arg->job                    = job;
        executor_arg->db                     = sleeper_arg->db;
        pthread_create(&id, NULL, executor, executor_arg);

        pthread_mutex_unlock(sleeper_arg->sll_lock);
    }

    return NULL;
}

static void close_pipes(union at_pipe* stdout_pipe, union at_pipe* stderr_pipe)
{
    int* fds[4] = {&stdout_pipe->named.rd, &stdout_pipe->named.wr, &stderr_pipe->named.rd, &stderr_pipe->named.wr};
    for(int i = 0; i < 4; i++) {
        if(*fds[i] == -1)
            continue;
        close(*fds[i]);
        *fds[i] = -1;
    }
}

static size_t read_pipe(int fd, char** buffer)
{
    *buffer = malloc(2048);

    size_t bytes_read = 0;
    ssize_t count;
    while((count = read(fd, *buffer + bytes_read, 2047)) > 0) {
        if(count == -1) {
            P_ERR("read() failed, child process output may not be shown, errno: %d (%s)", errno, strerror(errno));
            break;
        }

        if(count == 0) {
            // writing side of pipe is closed
            P_INFO_STR("finished reading from child process");
            break;
        }

        bytes_read += (size_t)count;

        *buffer = reallocarray(*buffer, 2048 + bytes_read, sizeof(char));
    }

    // fit the allocation down
    *buffer               = reallocarray(*buffer, bytes_read + 1, sizeof(char));
    (*buffer)[bytes_read] = 0; // we don't report that this 0 exists, but its how sqlite3_bind_text finds the length

    return bytes_read;
}

static void* thread_read_pipe(void* x)
{
    struct at_read_pipe_arg* arg = x;
    char* buffer                 = NULL;
    arg->bytes_read              = read_pipe(arg->fd, &buffer);
    return buffer;
}

static void create_and_join_pipe_threads(int stdout_fd, int stderr_fd, char** stdout_buffer, char** stderr_buffer)
{
    pthread_t stdout_id, stderr_id;

    struct at_read_pipe_arg stdout_arg = {.fd = stdout_fd};
    struct at_read_pipe_arg stderr_arg = {.fd = stderr_fd};

    pthread_create(&stdout_id, NULL, thread_read_pipe, &stdout_arg);
    pthread_create(&stderr_id, NULL, thread_read_pipe, &stderr_arg);

    pthread_join(stdout_id, (void**)stdout_buffer);
    pthread_join(stderr_id, (void**)stderr_buffer);
}

static const char* wait_for_job_status(pid_t child)
{
    int status;
    if(waitpid(child, &status, 0) == -1) {
        P_ERR("waitpid() failed, errno: %d (%s)", errno, strerror(errno));
        return JOB_UNKNOWN " - waitpid failed";
    }

    int exit_status;
    if(WIFEXITED(status)) {
        exit_status = WEXITSTATUS(status);
        P_INFO("exit status was %d", exit_status);
    } else {
        exit_status = -1;
    }

    return exit_status == 0 ? JOB_SUCCESS : JOB_FAILED;
}

static void print_db(sqlite3* db)
{
    char* data = NULL;
    if(get_all_from_db(db, &data)) {
        P_ERR_STR("Failed getting all jobs from database");
        return;
    }

    fprintf(stdout, "%s", data);
    fflush(stdout);
    free(data);
}

void* executor(void* x)
{
    P_INFO_STR("EXECUTING");

    struct at_executor_arg* executor_arg = x;

    union at_pipe stdout_pipe = {.array = {-1, -1}};
    union at_pipe stderr_pipe = {.array = {-1, -1}};

    if(pipe2(stdout_pipe.array, O_CLOEXEC) == -1) {
        P_ERR_STR("stdout pipe broke.");
        return (void*)EXIT_FAILURE;
    }

    if(pipe2(stderr_pipe.array, O_CLOEXEC) == -1) {
        close_pipes(&stdout_pipe, &stderr_pipe);
        P_ERR_STR("stderr pipe broke.");
        return (void*)EXIT_FAILURE;
    }

    pid_t child = fork();
    if(child == -1) {
        if(insert_job_output_into_db(executor_arg->db, "", "", JOB_FAILED_FORK, executor_arg->job->job_number) ==
           EXIT_FAILURE) {
            P_ERR_STR("couldn't insert failed job into db, fork() failed");
        }
        close_pipes(&stdout_pipe, &stderr_pipe);
        return (void*)EXIT_FAILURE;
    }

    if(child == 0) {
        dup2(stdout_pipe.named.wr, STDOUT_FILENO);
        dup2(stderr_pipe.named.wr, STDERR_FILENO);

        execl("/bin/sh", "sh", "-c", executor_arg->job->task, (char*)NULL);

        P_ERR("exec failed, errno: %d (%s)", errno, strerror(errno));

        exit(1);
    }

    /* close fds not required by parent */
    close(stdout_pipe.named.wr);
    close(stderr_pipe.named.wr);
    stdout_pipe.named.wr = -1;
    stderr_pipe.named.wr = -1;

    char* stdout_buffer = NULL;
    char* stderr_buffer = NULL;

    // we read in threads because its important to not block stderr while waiting for stdout
    // and vice-versa
    create_and_join_pipe_threads(stdout_pipe.named.rd, stderr_pipe.named.rd, &stdout_buffer, &stderr_buffer);

    const char* job_status = wait_for_job_status(child);

    P_INFO("%s", job_status);

    if(insert_job_output_into_db(executor_arg->db, stdout_buffer, stderr_buffer, job_status,
                                 executor_arg->job->job_number)) {
        P_ERR_STR("failed to insert job into db");
    }

    print_db(executor_arg->db);

    free(executor_arg->job);
    free(executor_arg);
    free(stdout_buffer);
    free(stderr_buffer);
    close_pipes(&stdout_pipe, &stderr_pipe);

    return (void*)EXIT_SUCCESS;
}
