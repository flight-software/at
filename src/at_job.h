#ifndef AT_JOB_H
#define AT_JOB_H

#include <pthread.h>
#include <time.h>

#include "libsll.h"

#define TASK_LENGTH 2048

struct at_job {
    int job_number;
    time_t time;
    char task[TASK_LENGTH];
};

typedef struct at_job at_job;

struct at_sleeper_arg {
    pthread_mutex_t* sll_lock;
    sll_fs* sorted_job_list;
    sqlite3* db;
};

typedef struct at_sleeper_arg at_sleeper_arg;

/**
 * This thread waits for SIGUSR1. Once received, it will
 * attempt to sleep until the earliest job is ready to be
 * executed and will then execute that job. This thread is not
 * expected to ever join.
 *
 * @param x Pointer to an at_sleeper_arg
 *
 * @return (void*)EXIT_SUCCESS else (void*)EXIT_FAILURE
 */
void* sleeper(void* x);

#endif
