#define _GNU_SOURCE

#include <sys/types.h>

#include <alloca.h>
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libdebug.h"
#include "sqlite3.h"

#include "at_db.h"

#define TABLE_NAME "task_database"

#define JOB_NUMBER_COL_NAME "JobNumber"
#define TASK_COL_NAME "Task"
#define EXEC_TIME_COL_NAME "ExecutionTime"
#define STDOUT_COL_NAME "Stdout"
#define STDERR_COL_NAME "Stderr"
#define STATUS_COL_NAME "Status"

#define JOB_NUMBER_COL_NUM 0
#define TASK_COL_NUM 1
#define EXEC_TIME_COL_NUM 2
#define STDOUT_COL_NUM 3
#define STDERR_COL_NUM 4
#define STATUS_COL_NUM 5

int open_database(const char* db_file, sqlite3** db)
{
    if(sqlite3_open(db_file, db) != SQLITE_OK) {
        P_ERR("Cannot open database: %s", sqlite3_errmsg(*db));
        close_database(db);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int create_database(sqlite3* db)
{
    const char* sql = "CREATE TABLE IF NOT EXISTS " TABLE_NAME "(" JOB_NUMBER_COL_NAME
                      " INTEGER PRIMARY KEY, " TASK_COL_NAME " VARCHAR(2048), " EXEC_TIME_COL_NAME
                      " INTEGER, " STDOUT_COL_NAME " TEXT, " STDERR_COL_NAME " TEXT, " STATUS_COL_NAME " TEXT);";
    char* err_msg = 0;

    if(sqlite3_exec(db, sql, NULL, NULL, &err_msg) != SQLITE_OK) {
        P_ERR("Failed to create database table %s", err_msg);
        sqlite3_free(err_msg);
        return EXIT_FAILURE;
    }
    P_INFO_STR("Table created or already created!");
    return EXIT_SUCCESS;
}

int close_database(sqlite3** db)
{
    if(sqlite3_close(*db) != SQLITE_OK) {
        P_ERR_STR("couldn't close db!");
        return EXIT_FAILURE;
    }
    *db = NULL;
    P_INFO_STR("closed db successfully!");
    return EXIT_SUCCESS;
}

static int count_files(const char* dir, size_t* file_count)
{
    DIR* dirp = opendir(dir);
    if(!dirp) {
        P_ERR("opendir(%s) failed, errno: %d (%s)", dir, errno, strerror(errno));
        return EXIT_FAILURE;
    }

    *file_count = 0;
    struct dirent* entry;

    while((entry = readdir(dirp))) {
        if(entry->d_type == DT_REG) { // If the entry is a regular file
            *file_count += 1;
        }
    }

    closedir(dirp);

    return EXIT_SUCCESS;
}

static void perform_archive(const char* db_dir, const char* db_file, size_t file_count)
{
    const char* fmt = "mv %s %s/destroyed%zu.db";

    size_t size = (size_t)snprintf(NULL, 0, fmt, db_file, db_dir, file_count);
    size++; // for '\0'

    char* cmd = alloca(size);

    snprintf(cmd, size, fmt, db_file, db_dir, file_count);

    int ret = system(cmd);
    P_INFO("system(%s) ret value is %d", cmd, ret);
}

int archive_database_file(const char* db_dir, const char* db_file)
{
    size_t file_count;
    if(count_files(db_dir, &file_count)) {
        P_ERR_STR("Failed to count files");
        return EXIT_FAILURE;
    }

    perform_archive(db_dir, db_file, file_count);

    return EXIT_SUCCESS;
}

int insert_task_into_db(sqlite3* db, const char* task, int time)
{
    const char* sql = "INSERT INTO " TABLE_NAME " (" TASK_COL_NAME ", " EXEC_TIME_COL_NAME ") VALUES(?, ?);";
    sqlite3_stmt* stmt;

    if(sqlite3_prepare_v2(db, sql, -1, &stmt, 0) != SQLITE_OK) {
        P_ERR("Cannot prepare statement: %s", sqlite3_errmsg(db));
        return EXIT_FAILURE;
    }

    int ret = EXIT_SUCCESS;

    if(sqlite3_bind_text(stmt, 1, task, -1, 0) != SQLITE_OK) {
        P_ERR("Cannot bind text: %s", sqlite3_errmsg(db));
        ret = EXIT_FAILURE;
        goto insert_task_into_db_ret;
    }

    if(sqlite3_bind_int(stmt, 2, time) != SQLITE_OK) {
        P_ERR("Cannot bind int: %s", sqlite3_errmsg(db));
        ret = EXIT_FAILURE;
        goto insert_task_into_db_ret;
    }

    if(sqlite3_step(stmt) != SQLITE_DONE) {
        P_ERR("Step was not done: %s", sqlite3_errmsg(db));
        ret = EXIT_FAILURE;
        goto insert_task_into_db_ret;
    }
    P_INFO_STR("insert good");

insert_task_into_db_ret:
    sqlite3_finalize(stmt);
    return ret;
}

/**
 * Retrieves column text or returns the "NULL" string
 * if no such column text exists.
 *
 * @param stmt Current sqlite statement
 * @param col_num Column number containing the text
 *
 * @return The string from the database or "NULL" if no such string
 */
static const char* col_text_or_null(sqlite3_stmt* stmt, int col_num)
{
    const char* s = (const char*)sqlite3_column_text(stmt, col_num);
    return s ? s : "NULL";
}

int get_single_job_from_db(sqlite3* db, int job_number, char** data)
{
    if(!data) {
        P_ERR_STR("data is NULL");
        return EXIT_FAILURE;
    }
    *data = NULL;

    const char* sql = "SELECT * FROM " TABLE_NAME " WHERE " JOB_NUMBER_COL_NAME "=?;";
    sqlite3_stmt* stmt;

    if(sqlite3_prepare_v2(db, sql, -1, &stmt, 0) != SQLITE_OK) {
        P_ERR("Cannot prepare statement: %s", sqlite3_errmsg(db));
        return EXIT_FAILURE;
    }

    if(sqlite3_bind_int(stmt, 1, job_number) != SQLITE_OK) {
        P_ERR("Cannot bind: %s", sqlite3_errmsg(db));
        sqlite3_finalize(stmt);
        return EXIT_FAILURE;
    }

    if(sqlite3_step(stmt) == SQLITE_ROW) {
        const char* fmt    = "%d,%s,%s,%s,%s,%s\n";
        int job            = sqlite3_column_int(stmt, JOB_NUMBER_COL_NUM);
        const char* text[] = {col_text_or_null(stmt, TASK_COL_NUM), col_text_or_null(stmt, EXEC_TIME_COL_NUM),
                              col_text_or_null(stmt, STDOUT_COL_NUM), col_text_or_null(stmt, STDERR_COL_NUM),
                              col_text_or_null(stmt, STATUS_COL_NUM)};
        size_t size        = (size_t)snprintf(NULL, 0, fmt, job, text[0], text[1], text[2], text[3], text[4]);
        size++; // for '\0'
        *data = malloc(size);
        snprintf(*data, size, fmt, job, text[0], text[1], text[2], text[3], text[4]);
    }

    sqlite3_finalize(stmt);
    return EXIT_SUCCESS;
}

struct print_status {
    char** buffer;
    size_t len;
};

static int print_db_cb(void* arg, int num_cols, char** col_text, char** col_name)
{
    struct print_status* ps = arg;
    for(int i = 0; i < num_cols; i++) {
        const char* name = col_name[i];
        const char* text = col_text[i] ? col_text[i] : "NULL";

        size_t size = (size_t)snprintf(NULL, 0, "%s = %s\n", name, text);
        size++; // for '\0'

        *ps->buffer = reallocarray(*ps->buffer, (ps->len + size) - 1, sizeof(char));

        snprintf((*ps->buffer + ps->len) - 1, size, "%s = %s\n", name, text);

        ps->len += size - 1;
    }
    return 0;
}

int get_all_from_db(sqlite3* db, char** data)
{
    if(!data) {
        P_ERR_STR("data is NULL");
        return EXIT_FAILURE;
    }

    const char* sql = "SELECT * FROM " TABLE_NAME ";";
    char* err_msg   = NULL;

    *data                  = calloc(1, sizeof(char));
    struct print_status ps = {
        .buffer = data,
        .len    = 1 // significantly reduces complexity in print_db_cb
    };

    if(sqlite3_exec(db, sql, print_db_cb, &ps, &err_msg) != SQLITE_OK) {
        P_ERR("Failed to get all data: %s", err_msg);

        sqlite3_free(err_msg);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int get_all_scheduled_from_db(sqlite3* db, char** data)
{
    if(!data) {
        P_ERR_STR("data is NULL");
        return EXIT_FAILURE;
    }

    const char* sql = "SELECT * FROM " TABLE_NAME " WHERE " STATUS_COL_NAME " IS NULL;";
    char* err_msg   = NULL;

    *data                  = calloc(1, sizeof(char));
    struct print_status ps = {
        .buffer = data,
        .len    = 1 // significantly reduces complexity in print_db_cb
    };

    if(sqlite3_exec(db, sql, print_db_cb, &ps, &err_msg) != SQLITE_OK) {
        P_ERR("Failed to get all data: %s", err_msg);

        sqlite3_free(err_msg);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int retrieve_all_jobs(sqlite3* db, at_job*** jobs, size_t* len)
{
    if(!jobs) {
        P_ERR_STR("jobs is NULL");
        return EXIT_FAILURE;
    }

    if(!len) {
        P_ERR_STR("len is NULL");
    }

    *jobs = NULL;
    *len  = 0;

    const char* sql = "SELECT " JOB_NUMBER_COL_NAME ", " TASK_COL_NAME ", " EXEC_TIME_COL_NAME " FROM " TABLE_NAME
                      " WHERE " STATUS_COL_NAME " IS NULL;";
    sqlite3_stmt* stmt;

    if(sqlite3_prepare_v2(db, sql, -1, &stmt, 0) != SQLITE_OK) {
        P_ERR("Cannot prepare statement: %s", sqlite3_errmsg(db));
        return EXIT_FAILURE;
    }

    while(sqlite3_step(stmt) == SQLITE_ROW) {
        at_job* job = calloc(1, sizeof(at_job));

        job->job_number = sqlite3_column_int(stmt, 0);
        job->time       = sqlite3_column_int(stmt, 2);
        memcpy(job->task, sqlite3_column_text(stmt, 1), TASK_LENGTH);

        *jobs         = reallocarray(*jobs, *len + 1, sizeof(at_job*));
        (*jobs)[*len] = job;
        *len += 1;
    }

    sqlite3_finalize(stmt);
    return EXIT_SUCCESS;
}

int insert_job_output_into_db(sqlite3* db, const char* stdout_buffer, const char* stderr_buffer, const char* status,
                              int job_number)
{
    const char* sql = "UPDATE " TABLE_NAME " SET " STDOUT_COL_NAME "=?, " STDERR_COL_NAME "=?, " STATUS_COL_NAME
                      "=? WHERE " JOB_NUMBER_COL_NAME "=?;";
    sqlite3_stmt* stmt;

    if(sqlite3_prepare_v2(db, sql, -1, &stmt, 0) != SQLITE_OK) {
        P_ERR("Cannot prepare statement: %s", sqlite3_errmsg(db));
        return EXIT_FAILURE;
    }

    int ret = EXIT_SUCCESS;

    if(sqlite3_bind_text(stmt, 1, stdout_buffer, -1, 0)) {
        P_ERR("Cannot bind: %s", sqlite3_errmsg(db));
        ret = EXIT_FAILURE;
        goto insert_job_output_into_db_ret;
    }

    if(sqlite3_bind_text(stmt, 2, stderr_buffer, -1, 0)) {
        P_ERR("Cannot bind: %s", sqlite3_errmsg(db));
        ret = EXIT_FAILURE;
        goto insert_job_output_into_db_ret;
    }

    if(sqlite3_bind_text(stmt, 3, status, -1, 0)) {
        P_ERR("Cannot bind: %s", sqlite3_errmsg(db));
        ret = EXIT_FAILURE;
        goto insert_job_output_into_db_ret;
    }

    if(sqlite3_bind_int(stmt, 4, job_number)) {
        P_ERR("Cannot bind: %s", sqlite3_errmsg(db));
        ret = EXIT_FAILURE;
        goto insert_job_output_into_db_ret;
    }

    if(sqlite3_step(stmt) != SQLITE_DONE) {
        P_ERR("Failed running stmt: %s", sqlite3_errmsg(db));
        ret = EXIT_FAILURE;
        goto insert_job_output_into_db_ret;
    }

    P_INFO("insert good for job %d", job_number);

insert_job_output_into_db_ret:
    sqlite3_finalize(stmt);
    return ret;
}

int remove_job_from_db(sqlite3* db, int job_number)
{
    const char* sql = "UPDATE " TABLE_NAME " SET " STATUS_COL_NAME "='Removed' WHERE " JOB_NUMBER_COL_NAME "=?;";
    sqlite3_stmt* stmt;

    if(sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
        P_ERR("Cannot prepare statement: %s", sqlite3_errmsg(db));
        return EXIT_FAILURE;
    }

    int ret = EXIT_SUCCESS;

    if(sqlite3_bind_int(stmt, 1, job_number) != SQLITE_OK) {
        P_ERR("Cannot bind: %s", sqlite3_errmsg(db));
        ret = EXIT_FAILURE;
        goto remove_job_from_db_ret;
    }

    if(sqlite3_step(stmt) != SQLITE_DONE) {
        P_ERR("Failed running stmt: %s", sqlite3_errmsg(db));
        ret = EXIT_FAILURE;
    }

remove_job_from_db_ret:
    sqlite3_finalize(stmt);
    return ret;
}

int is_job_finished(sqlite3* db, int job_number)
{
    const char* sql = "SELECT * FROM " TABLE_NAME " WHERE " JOB_NUMBER_COL_NAME "=?;";
    sqlite3_stmt* stmt;

    if(sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
        P_ERR("Cannot prepare statement: %s", sqlite3_errmsg(db));
        return EXIT_FAILURE;
    }

    int ret = 1;

    if(sqlite3_bind_int(stmt, 1, job_number) != SQLITE_OK) {
        P_ERR("Cannot bind: %s", sqlite3_errmsg(db));
        ret = -1;
        goto is_job_finished_ret;
    }

    if(sqlite3_step(stmt) != SQLITE_ROW) {
        P_ERR("Failed running stmt: %s", sqlite3_errmsg(db));
        ret = -1;
        goto is_job_finished_ret;
    }

    if(sqlite3_column_text(stmt, 5) == NULL) {
        P_INFO_STR("status is NULL, job isnt finished");
        ret = 0;
    }

is_job_finished_ret:
    sqlite3_finalize(stmt);
    return ret;
}

int get_last_row_id(sqlite3* db)
{
    const char* sql = "SELECT MAX(" JOB_NUMBER_COL_NAME ") FROM " TABLE_NAME ";";
    sqlite3_stmt* stmt;

    if(sqlite3_prepare_v2(db, sql, -1, &stmt, 0) != SQLITE_OK) {
        P_ERR("Cannot prepare statement: %s", sqlite3_errmsg(db));
        return -1;
    }

    int row_id = 0;
    if(sqlite3_step(stmt) != SQLITE_ROW) {
        P_INFO_STR("no data");
        row_id = -1;
    } else {
        row_id = sqlite3_column_int(stmt, 0);
    }

    sqlite3_finalize(stmt);

    return row_id;
}

int need_to_recreate(sqlite3* db)
{
    const char* sql = "SELECT * FROM " TABLE_NAME " WHERE " STATUS_COL_NAME " IS NULL;";
    sqlite3_stmt* stmt;

    if(sqlite3_prepare_v2(db, sql, -1, &stmt, 0) != SQLITE_OK) {
        P_ERR("Cannot prepare statement: %s", sqlite3_errmsg(db));
        return 0;
    }

    int recreate = sqlite3_step(stmt) == SQLITE_ROW;

    sqlite3_finalize(stmt);

    return recreate;
}
