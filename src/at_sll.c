#include <inttypes.h>
#include <pthread.h>
#include <stdint.h>
#include <time.h>

#include "libdebug.h"
#include "libsll.h"
#include "sqlite3.h"

#include "at_db.h"
#include "at_job.h"
#include "at_sll.h"

int sll_sort_job_cb(const void* a, const void* b, void* arg)
{
    (void)arg;
    time_t t1 = ((const at_job*)a)->time;
    time_t t2 = ((const at_job*)b)->time;
    if(t1 < t2)
        return -1;
    if(t1 == t2)
        return 0;
    return 1;
}

uint8_t sll_search_job_cb(void* cur, void* looking_for)
{
    int cur_job_number      = ((at_job*)cur)->job_number;
    int look_for_job_number = *((int*)looking_for);
    return (cur_job_number == look_for_job_number);
}

int recreate_sll(sqlite3* db, sll_fs* sorted_job_list, pthread_mutex_t* sll_lock)
{
    pthread_mutex_lock(sll_lock);

    sll_clear(sorted_job_list); // just to be safe

    at_job** jobs = NULL;
    size_t len;
    if(retrieve_all_jobs(db, &jobs, &len)) {
        P_ERR_STR("Failed to retrieve jobs");
        return EXIT_FAILURE;
    }

    for(size_t i = 0; i < len; i++) {
        at_job* job = jobs[i];
        P_INFO("number, time, & task: %d %" PRId32 " %s", job->job_number, (int32_t)job->time, job->task);

        if(job->time < time(NULL)) {
            P_ERR("time is too late, supposed to execute at %" PRId32 ", current time is %" PRId32, (int32_t)job->time,
                  (int32_t)time(NULL));
            if(insert_job_output_into_db(db, "", "", JOB_FAILED_TIME_PASSED, job->job_number)) {
                P_ERR("couldn't insert failed job %d into db", job->job_number);
            }
            free(job);
            continue;
        }

        if(sll_push_tail(sorted_job_list, job, sizeof(at_job), SLL_NOWRITE)) {
            insert_job_output_into_db(db, "", "", "Failed - couldn't rebuild into sll", job->job_number);
            P_ERR_STR("sll push tail failed");
            sll_clear(sorted_job_list);
            for(; i < len; i++) {
                free(jobs[i]);
            }
            free(jobs);
            pthread_mutex_unlock(sll_lock);
            return EXIT_FAILURE;
        }
    }

    free(jobs);

    sll_sort(sorted_job_list, sll_sort_job_cb, NULL);

    pthread_mutex_unlock(sll_lock);

    return EXIT_SUCCESS;
}
