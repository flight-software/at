#include <sys/stat.h>

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "at.h"
#include "libdaemon.h"
#include "libdebug.h"
#include "librpc.h"
#include "sqlite3.h"

int main(int argc, char* argv[])
{
    (void)argc;
    (void)argv;
    // int x = system("sudo sqlite3 test2.db -echo \'select * from task_database;\'");
    int x =
        system("sudo sqlite3 test2.db -echo \"SELECT name FROM sqlite_master WHERE type=\'table\' ORDER BY name;\"");
    printf("x is %d\n", x);

    return 0;
}
