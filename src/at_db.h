#ifndef AT_DB_H
#define AT_DB_H

#include "sqlite3.h"

#include "at_job.h"

#define AT_DB_DIR "/var/log/at"
#define AT_DB_FILE AT_DB_DIR "/0.db"

#define JOB_SUCCESS "Job succeeded"
#define JOB_FAILED "Job failed"
#define JOB_UNKNOWN "Job unknown"
#define JOB_FAILED_TIME_PASSED JOB_FAILED " - Time Passed"
#define JOB_FAILED_FORK JOB_FAILED " - Fork Failed"

/**
 * Opens the sqlite3 database. Does not deal with filesystem issues.
 *
 * @param db_file Full path to database file which must exist and be R/W
 * @param db Pointer to a pointer to the sqlite3 database
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int open_database(const char* db_file, sqlite3** db);

/**
 * Creates the table in an opened database.
 * Does not close the database.
 *
 * @param db Pointer to the sqlite3 database
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int create_database(sqlite3* db);

/**
 * Attempts to close the sqlite3 database.
 *
 * @param db Pointer to the sqlite3 database
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int close_database(sqlite3** db);

/**
 * Moves the current database file to a uniquely
 * numbered archive file.
 *
 * @param db_dir The directory that all at database files exist in
 * @param db_file The full path to the current database file
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int archive_database_file(const char* db_dir, const char* db_file);

/**
 * Insert a new task into the database.
 *
 * @param db Pointer to the sqlite3 database
 * @param task String task to be executed at time
 * @param time The Unix time in seconds when the task shall be executed
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int insert_task_into_db(sqlite3* db, const char* task, int time);

/**
 * Retrieve the job from the database and generate
 * a formatted string of the data.
 *
 * @param db Pointer to the sqlite3 database
 * @param job_number Job number to pull data for
 * @param data Formatted string. Data will be allocated with malloc()
 * and should be free'd with free(). On error will be set to NULL
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int get_single_job_from_db(sqlite3* db, int job_number, char** data);

/**
 * Retrieve all jobs from the database and generate
 * a formatted string of the data.
 *
 * @param db Pointer to the sqlite3 database
 * @param data Formatted string. Data will be allocated with malloc()
 * and should be free'd with free(). On error will be NULL
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int get_all_from_db(sqlite3* db, char** data);

/**
 * Retrieve all scheduled jobs from the database and generate
 * a formatted string of the data.
 *
 * @param db Pointer to the sqlite3 database
 * @param data Formatted string. Data will be allocated with malloc()
 * and should be free'd with free(). On error will be NULL
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int get_all_scheduled_from_db(sqlite3* db, char** data);

/**
 * Retrieve all of the job data from the database into an array.
 *
 * @param db Pointer to the sqlite3 database
 * @param jobs Job pointer array and each element will be allocated
 * with malloc() and should be free'd with free().
 * On error will be NULL
 * @param len Length of job array
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int retrieve_all_jobs(sqlite3* db, at_job*** jobs, size_t* len);

/**
 * Insert the result of a job into the database.
 *
 * @param db Pointer to the sqlite3 database
 * @param stdout_buffer Data from stdout
 * @param stderr_buffer Data from stderr
 * @param status Status message of job
 * @param job_number Job number to remove from the database
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int insert_job_output_into_db(sqlite3* db, const char* stdout_buffer, const char* stderr_buffer, const char* status,
                              int job_number);

/**
 * Remove a job from the database.
 *
 * @param db Pointer to the sqlite3 database
 * @param job_number Job number to remove from the database
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE;
 */
int remove_job_from_db(sqlite3* db, int job_number);

/**
 * Determine if a job has executed.
 *
 * @param db Pointer to the sqlite3 database
 * @param job_number Job number to determine execution status of
 *
 * @return 0 if job has not finished, positive if job finished, else -1
 */
int is_job_finished(sqlite3* db, int job_number);

/**
 * Retrieve the highest numbered job id from the database.
 *
 * @param db Pointer to the sqlite3 database
 *
 * @return Job id else -1
 */
int get_last_row_id(sqlite3* db);

/**
 * Determines if the database contains unfinished jobs.
 * Use when starting up the at daemon to detect errors such as
 * unexpected poweroff.
 *
 * @param db Pointer to the sqlite3 database
 *
 * @return 1 if unfinished jobs or 0 if no need to recreate
 */
int need_to_recreate(sqlite3* db);

#endif
