#ifndef AT_RPC_H
#define AT_RPC_H

#include <stdlib.h>
#include <time.h>

#include "libdebug.h"
#include "sqlite3.h"
#include "at.h"

#define ADD_JOB_MESSAGE            UINT16_C(37)
#define REMOVE_JOB_MESSAGE         UINT16_C(38)
#define CLEAR_SLL_MESSAGE          UINT16_C(39)
#define CLOSE_DB_MESSAGE           UINT16_C(40)
#define OPEN_AND_CREATE_DB_MESSAGE UINT16_C(41)

/**
 * Sets up the RPC server and keeps track of needed pointers.
 *
 * @param db Pointer to a pointer to the sqlite3 database
 * @param sorted_job_list Pointer to the SLL managing jobs
 * @param sll_lock Pointer to the mutex protecting the SLL
 * @param tid Pthread ID of the "sleeper" thread so that adding new jobs can cause a signal
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int at_rpc_init(sqlite3** db, sll_fs* sorted_job_list, pthread_mutex_t* sll_lock, pthread_t* tid);

/**
 * Formats jobs coming from the command line into a task and adds the job to the SLL.
 *
 * @param argv Command line argument array starting from the first task string
 * @param argc Number of pointers in argv for task strings
 * @param task_date Time of execution of task
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int send_task_rpc(const char* const* argv, size_t argc, const struct tm* task_date);

/**
 * Remove a job from the SLL.
 *
 * @param job_number Job ID number
 *
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int send_remove_task_rpc(int job_number);

/**
 * Trigger a complete clearing of the current SLL.
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int send_clear_sll_rpc(void);

/**
 * Trigger closing of the sqlite3 database
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int send_close_db_rpc(void);

/**
 * Trigger re-opening of the sqlite3 database
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int send_open_and_create_db_rpc(void);

#endif
