#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "libdaemon.h"
#include "libdebug.h"
#include "librpc.h"
#include "libsll.h"
#include "sqlite3.h"

#include "at_db.h"
#include "at_job.h"
#include "at_rpc.h"
#include "at_sll.h"
#include "at.h"

/*
Details:
 - A database stores all schedueled jobs, finished jobs, and jobs that crashed in the order they were scheduled (not in
execution order)
 - A sll stores all currently schedueled jobs in order of when they should execute (soonest to latest)
 - Max:
  - 1000 jobs queued at a time (in sll at a time)
  - 99 old database files

Compilation things
 - compile sqlite3 with SQLITE_THREADSAFE=1 for it to be threadsafe - both executor and message receiver use the
database
  - https://www.sqlite.org/faq.html#q6
 - check rpc ports that they don't overlap with another program

N
 - overflow & underflow ??

*/

static void cleanup(void);
static int setup_daemon(void);
static time_t convert_to_seconds(time_t N, const char* unit);
static int add_relative_job(int argc, char** argv);
static int add_absolute_job(int argc, char** argv);
static int do_info(const char* job_num_str);
static int do_jobs(void);
static int do_all(void);
static int do_remove(const char* job_num_str);
static int do_destroy(void);

static sll_fs sorted_job_list;
static pthread_mutex_t sll_lock;
static pthread_t sleeper_id;

static sqlite3* db;

static void cleanup(void) { pthread_mutex_destroy(&sll_lock); }

static int setup_daemon(void)
{
    // Daemon setup
    daemonize();

    // Linked List Setup
    if(sll_init(&sorted_job_list, NULL)) {
        P_ERR_STR("sll_init() failed");
        return EXIT_FAILURE;
    }

    // Timezone environment variable setup
    if(setenv("TZ", "UTC", 1)) {
        P_ERR("setenv failed, errno: %d, (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }
    tzset();

    // /var/log/at directory setup - need database to be created here
    if(mkdir(AT_DB_DIR, S_IRWXU | S_IRWXG | S_IRWXO) && errno != EEXIST) {
        P_ERR("Failed to make directory %s, errno: %d (%s)", AT_DB_DIR, errno, strerror(errno));
        return EXIT_FAILURE;
    }
    errno = 0;

    // Database file setup
    if(access(AT_DB_FILE, F_OK)) {
        // database file doesn't exist so creating it - first time running program
        P_INFO("database file doesnt exist so creating one: %s", AT_DB_FILE);
        FILE* db_file = fopen(AT_DB_FILE, "w");
        if(fclose(db_file) == EOF) {
            P_ERR("could not close database file after creating it, errno: %d (%s)", errno, strerror(errno));
            return EXIT_FAILURE;
        }
    }

    // Database setup
    if(open_database(AT_DB_FILE, &db)) {
        P_ERR_STR("Failed to open database");
        return EXIT_FAILURE;
    }

    if(create_database(db)) {
        P_ERR_STR("Failed to create database");
        close_database(&db);
        return EXIT_FAILURE;
    }

    // recreate linked list and database if failed
    if(need_to_recreate(db)) {
        P_INFO_STR("Need to recreate old sll");
        if(recreate_sll(db, &sorted_job_list, &sll_lock)) {
            P_ERR_STR("recreating sll failed");
            return EXIT_FAILURE;
        }
    } else {
        P_INFO_STR("No need to recreate sll, all jobs finished");
    }

    if(at_rpc_init(&db, &sorted_job_list, &sll_lock, &sleeper_id)) {
        P_ERR_STR("rpc server failed to start");
        return EXIT_FAILURE;
    }

    // Thread setup
    static at_sleeper_arg sleeper_arg;
    sleeper_arg = (at_sleeper_arg){.sll_lock = &sll_lock, .sorted_job_list = &sorted_job_list, .db = db};
    if(pthread_create(&sleeper_id, NULL, sleeper, &sleeper_arg)) {
        P_ERR("the sleeper thread failed to start, errno: %d (%s)", errno, strerror(errno));
        return EXIT_FAILURE;
    }

    pthread_exit((void*)EXIT_SUCCESS);
    // Done setting up
}

static time_t convert_to_seconds(time_t N, const char* unit)
{
    if(!strcmp(unit, "hours"))
        return N * 3600;
    if(!strcmp(unit, "minutes"))
        return N * 60;
    if(!strcmp(unit, "seconds"))
        return N;
    return 0;
}

static int add_relative_job(int argc, char** argv)
{
    size_t arg_index      = 1;
    time_t offset_seconds = 0;

    while(argv[arg_index][0] == '+') {      // assume no task starts with +
        if(arg_index >= (size_t)argc - 2) { // should never get to the last part of command
            P_ERR_STR("Input is bad");
            return EXIT_FAILURE;
        }

        time_t number_read = (time_t)strtol(argv[arg_index], NULL, 10);
        if(number_read < 1 || number_read > 100000) {
            P_ERR("%" PRId64 "is out of range", (int64_t)number_read);
            return EXIT_FAILURE;
        }
        offset_seconds += convert_to_seconds(number_read, argv[arg_index + 1]);
        arg_index += 2;
    }

    time_t total_seconds = time(NULL) + offset_seconds;
    struct tm* task_date = gmtime(&total_seconds);
    if(!task_date) {
        P_ERR("gmtime() failed: %s", strerror(errno));
        return EXIT_FAILURE;
    }

    if(send_task_rpc((const char* const*)(argv + arg_index), (size_t)argc - arg_index, task_date)) {
        P_ERR_STR("failed to send task to rpc");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

static int add_absolute_job(int argc, char** argv)
{
    size_t colon_count = 0;

    {
        const char* arg = argv[1];
        while(*arg) {
            if(*arg == ':')
                colon_count++;
            arg++;
        }
        if(colon_count >= 6) {
            P_ERR_STR("More than 6 time components specified");
            return EXIT_FAILURE;
        }
    }

    struct date_components {
        int16_t year;
        int16_t month;
        int16_t day;
        int16_t hour;
        int16_t minute;
        int16_t second;
    };

    struct date_components date = {0};

    {
        int16_t* date_arr = (int16_t*)&date;
        date_arr += 5 - colon_count; // 0 colons, start at second
                                     // 5 colons, start at year
        const char* component = strtok(argv[1], ":");
        while(component) {
            *date_arr = (int16_t)strtol(component, NULL, 10);
            date_arr++;
            component = strtok(NULL, ":");
        }
    }

    struct tm* task_date = gmtime(&(time_t){time(NULL)});

    if(task_date == NULL) {
        P_ERR_STR("gmtime() failed");
        return EXIT_FAILURE;
    }

    task_date->tm_isdst = -1;

    switch(colon_count) {
    case 5:
        if(date.year >= 2038) {
            P_ERR_STR("Cannot set year past 2038");
            return EXIT_FAILURE;
        }
        task_date->tm_year = date.year - 1900;
        // fall through
    case 4:
        if(date.month < 1 || date.month > 12) {
            P_ERR_STR("Month outside of [1-12]");
            return EXIT_FAILURE;
        }
        task_date->tm_mon = date.month - 1;
        // fall through
    case 3:
        if(date.day < 1 || date.day > 31) {
            P_ERR_STR("Day outside of [1-31]");
            return EXIT_FAILURE;
        }
        task_date->tm_mday = date.day;
        // fall through
    case 2:
        if(date.hour < 0 || date.hour > 23) {
            P_ERR_STR("Hour outside of [0-23]");
            return EXIT_FAILURE;
        }
        task_date->tm_hour = date.hour;
        // fall through
    case 1:
        if(date.minute < 0 || date.minute > 59) {
            P_ERR_STR("Minute outside of [0-59]");
            return EXIT_FAILURE;
        }
        task_date->tm_min = date.minute;
        // fall through
    case 0:
        if(date.second < 0 || date.second > 59) {
            P_ERR_STR("Second outside of [0-59]");
            return EXIT_FAILURE;
        }
        task_date->tm_sec = date.second;
        break;
    default:
        break;
    }

    mktime(task_date); // fills in tm_wday, tm_yday based off of other info

    if(send_task_rpc((const char* const*)(argv + 2), (size_t)argc - 2, task_date)) {
        P_ERR_STR("failed to send task to rpc");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

static int do_info(const char* job_num_str)
{
    int job_number = (int)strtol(job_num_str, NULL, 10);

    if(open_database(AT_DB_FILE, &db)) {
        P_ERR_STR("Failed to open database");
        return EXIT_FAILURE;
    }

    char* data = NULL;
    if(get_single_job_from_db(db, job_number, &data)) {
        P_ERR("failed to get info on job %d", job_number);
        close_database(&db);
        return EXIT_FAILURE;
    }

    fprintf(stdout, "%s", data);
    fflush(stdout);

    free(data);

    close_database(&db);

    return EXIT_SUCCESS;
}

static int do_jobs(void)
{
    if(open_database(AT_DB_FILE, &db)) {
        P_ERR_STR("Failed to open database");
        return EXIT_FAILURE;
    }

    char* data = NULL;
    if(get_all_scheduled_from_db(db, &data)) {
        P_ERR_STR("failed to get info on schedules jobs");
        close_database(&db);
        return EXIT_FAILURE;
    }

    fprintf(stdout, "%s", data);
    fflush(stdout);

    free(data);

    close_database(&db);

    return EXIT_SUCCESS;
}

static int do_all(void)
{
    if(open_database(AT_DB_FILE, &db)) {
        P_ERR_STR("Failed to open database");
        return EXIT_FAILURE;
    }

    char* data = NULL;
    if(get_all_from_db(db, &data)) {
        P_ERR_STR("Failed getting all jobs from database");
        close_database(&db);
        return EXIT_FAILURE;
    }

    fprintf(stdout, "%s", data);
    fflush(stdout);

    free(data);

    close_database(&db);

    return EXIT_SUCCESS;
}

static int do_remove(const char* job_num_str)
{
    int job_number = (int)strtol(job_num_str, NULL, 10);
    if(send_remove_task_rpc(job_number)) {
        P_ERR_STR("Send remove task failed");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

static int do_destroy(void)
{
    if(send_clear_sll_rpc()) {
        P_ERR_STR("send_clear_sll() failed for destroy command, still using old database");
        return EXIT_FAILURE;
    }
    if(send_close_db_rpc()) {
        P_ERR_STR("destroy failed");
        return EXIT_FAILURE;
    }
    if(archive_database_file(AT_DB_DIR, AT_DB_FILE)) {
        P_ERR_STR("destroy failed, couldn't move files, please restart at to continue using");
        return EXIT_FAILURE;
    }
    if(send_open_and_create_db_rpc()) {
        P_ERR_STR("destroy failed, couldn't create new database, please restart at to continue using");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int main(int argc, char** argv)
{
    atexit(cleanup);

    if(argc <= 1) {
        return setup_daemon();
    }

    if(argv[1][0] == '+') {
        if(argc < 4) {
            P_ERR_STR("Not enough arguments");
            return EXIT_FAILURE;
        }

        return add_relative_job(argc, argv);
    }

    if(isdigit(argv[1][0])) {
        if(argc < 3) {
            P_ERR_STR("Not enough arguments");
            return EXIT_FAILURE;
        }

        return add_absolute_job(argc, argv);
    }

    if(!strcmp(argv[1], "info")) {
        if(argc < 3) {
            P_ERR_STR("Not enough arguments");
            return EXIT_FAILURE;
        }

        return do_info(argv[2]);
    }

    if(!strcmp(argv[1], "jobs")) {
        return do_jobs();
    }

    if(!strcmp(argv[1], "all")) {
        return do_all();
    }

    if(!strcmp(argv[1], "remove")) {
        if(argc < 3) {
            P_ERR_STR("Not enough arguments");
            return EXIT_FAILURE;
        }

        return do_remove(argv[2]);
    }

    if(!strcmp(argv[1], "destroy")) {
        return do_destroy();
    }

    P_ERR_STR("Valid command was not given");

    return EXIT_FAILURE;
}
