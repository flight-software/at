#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "libdebug.h"
#include "librpc.h"
#include "libsll.h"
#include "sqlite3.h"

#include "at_db.h"
#include "at_job.h"
#include "at_rpc.h"
#include "at_sll.h"

struct at_rpc_arg {
    sll_fs* sorted_job_list;
    sqlite3** db;
};

static rpc_server_fs rpc_server;
static pthread_t* sleeper_id;

static int message_receiver(const void* in, size_t in_len, void* out, size_t* out_len, void* param)
{
    (void)out;
    (void)out_len;

    if(in_len != TASK_LENGTH + sizeof(struct tm)) {
        P_ERR("in len was %zu bytes, should be %zu bytes", in_len, TASK_LENGTH + sizeof(struct tm));
        return EXIT_FAILURE;
    }

    struct at_rpc_arg* rpc_arg = param;

    struct tm t;
    memcpy(&t, (const uint8_t*)in + TASK_LENGTH, sizeof(struct tm));

    // the time the task should be executed as a string
    char time_string[26];
    asctime_r(&t, time_string); // does not alter t
    time_string[24] = '\t';

    P_INFO("time string: %s", time_string);

    // the time the task should be executed as an int
    time_t execution_time = mktime(&t);

    P_INFO_STR("MESSAGE RECEIVED.");
    P_INFO("SLEEP AMOUNT IS %" PRId32, (int32_t)execution_time);

    // fill job struct
    at_job* job = calloc(sizeof(at_job), 1);
    strncpy(job->task, in, TASK_LENGTH);
    job->job_number = get_last_row_id(*rpc_arg->db) + 1;
    job->time       = execution_time;

    if(insert_task_into_db(*rpc_arg->db, job->task, (int)job->time)) {
        P_ERR_STR("Couldn't insert job into the database");
        free(job);
        return EXIT_FAILURE;
    }

    if(execution_time < time(NULL)) {
        P_ERR("time is too late, supposed to execute at %" PRIu32 ", current time is %" PRId32,
              (uint32_t)execution_time, (int32_t)time(NULL));
        // job failed
        if(insert_job_output_into_db(*rpc_arg->db, "", "", JOB_FAILED_TIME_PASSED, job->job_number)) {
            P_ERR_STR("couldn't insert failed job into db");
            free(job);
            return EXIT_FAILURE;
        }
        free(job);
        return EXIT_SUCCESS;
    }

    sll_insert_sorted(rpc_arg->sorted_job_list, job, sizeof(at_job), SLL_NOWRITE, sll_sort_job_cb, NULL);
    int is_head = job == sll_peek_head(rpc_arg->sorted_job_list);

    if(is_head) {
        if(pthread_kill(*sleeper_id, SIGUSR1)) {
            P_ERR("pthread kill failed, errno: %d (%s)", errno, strerror(errno));
            return EXIT_FAILURE;
        }
        P_INFO_STR("sent sigusr1");
    }

    return EXIT_SUCCESS;
}

static int remove_message_receiver(const void* in, size_t in_len, void* out, size_t* out_len, void* param)
{
    (void)out;
    (void)out_len;

    if(in_len != sizeof(int)) {
        P_ERR("in len was %zu bytes, should be %zu bytes", in_len, sizeof(int));
        return EXIT_FAILURE;
    }

    struct at_rpc_arg* rpc_arg = param;

    int job_number;
    memcpy(&job_number, in, sizeof(int));

    int highest_job_number = get_last_row_id(*rpc_arg->db);

    if(job_number > highest_job_number) {
        P_ERR("removing job %d failed, highest job number is %d", job_number, highest_job_number);
        return EXIT_FAILURE;
    }

    int r = is_job_finished(*rpc_arg->db, job_number);
    if(r == -1) {
        P_ERR("Failed to determine if job number %d has finished", job_number);
        return EXIT_FAILURE;
    }

    if(r > 0) {
        P_ERR("job number %d has already finished", job_number);
        return EXIT_FAILURE;
    }

    at_job* to_remove = sll_search(rpc_arg->sorted_job_list, sll_search_job_cb, &job_number);
    if(to_remove) {
        P_INFO_STR("sll search was successful");

        int need_to_wakeup;
        {
            at_job* head   = sll_peek_head(rpc_arg->sorted_job_list);
            need_to_wakeup = (head == to_remove) ? 1 : 0;
        }

        if(sll_pull_ptr(rpc_arg->sorted_job_list, to_remove)) {
            P_ERR("Failed to remove job %d", job_number);
            return EXIT_FAILURE;
        }
        P_INFO("Removed job %d successfully!", job_number);

        if(need_to_wakeup) {
            if(pthread_kill(*sleeper_id, SIGUSR1)) {
                P_ERR("pthread kill failed, errno: %d (%s)", errno, strerror(errno));
                return EXIT_FAILURE;
            }
            P_INFO_STR("sent sigusr1");
        }
    } else {
        P_ERR("sll search failed, failed to remove job %d", job_number);
        return EXIT_FAILURE;
    }

    if(remove_job_from_db(*rpc_arg->db, job_number)) {
        P_ERR("Failed to remove job %d from the db", job_number);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

static int clear_sll_message_receiver(const void* in, size_t in_len, void* out, size_t* out_len, void* param)
{
    (void)in;
    (void)in_len;
    (void)out;
    (void)out_len;

    struct at_rpc_arg* rpc_arg = param;

    sll_clear(rpc_arg->sorted_job_list); // no way for this function to fail in libsll.c

    return EXIT_SUCCESS;
}

static int close_db_message_receiver(const void* in, size_t in_len, void* out, size_t* out_len, void* param)
{
    (void)in;
    (void)in_len;
    (void)out;
    (void)out_len;

    struct at_rpc_arg* rpc_arg = param;

    close_database(rpc_arg->db);

    return EXIT_SUCCESS;
}

static int open_and_create_message_receiver(const void* in, size_t in_len, void* out, size_t* out_len, void* param)
{
    (void)in;
    (void)in_len;
    (void)out;
    (void)out_len;

    struct at_rpc_arg* rpc_arg = param;

    if(open_database(AT_DB_FILE, rpc_arg->db)) {
        P_ERR_STR("couldn't open database");
        return EXIT_FAILURE;
    }
    if(create_database(*rpc_arg->db)) {
        P_ERR_STR("couldn't create table in database");
        close_database(rpc_arg->db);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int at_rpc_init(sqlite3** db, sll_fs* sorted_job_list, pthread_mutex_t* sll_lock, pthread_t* tid)
{
    sleeper_id = tid;

    // startup up server
    if(rpc_server_init(&rpc_server, AT_RPC_SOCKET, RPC_SERVER_MT)) {
        P_ERR_STR("SERVER INIT FAILED");
        return EXIT_FAILURE;
    }

    static struct at_rpc_arg rpc_arg;

    rpc_arg = (struct at_rpc_arg){.sorted_job_list = sorted_job_list, .db = db};

    rpc_server_add_elem(&rpc_server, ADD_JOB_MESSAGE, message_receiver, &rpc_arg, sll_lock);
    rpc_server_add_elem(&rpc_server, REMOVE_JOB_MESSAGE, remove_message_receiver, &rpc_arg, sll_lock);
    rpc_server_add_elem(&rpc_server, CLEAR_SLL_MESSAGE, clear_sll_message_receiver, &rpc_arg, sll_lock);
    rpc_server_add_elem(&rpc_server, CLOSE_DB_MESSAGE, close_db_message_receiver, &rpc_arg, sll_lock);
    rpc_server_add_elem(&rpc_server, OPEN_AND_CREATE_DB_MESSAGE, open_and_create_message_receiver, &rpc_arg, sll_lock);

    return EXIT_SUCCESS;
}

int send_task_rpc(const char* const* argv, size_t argc, const struct tm* task_date)
{
    char task[TASK_LENGTH]; // max length of task is 2kb
    task[0]           = 0;
    size_t offset     = 0;
    int_fast8_t first = 1;

    for(size_t j = 0; j < argc; j++) {
        size_t to_write = TASK_LENGTH - offset;
        ssize_t r       = (ssize_t)snprintf(task + offset, to_write, first ? "%s" : " %s", argv[j]);
        first           = 0;
        if(r < 0) {
            P_ERR("snprintf failed, errno: %d (%s)", errno, strerror(errno));
            return EXIT_FAILURE;
        }
        if(to_write + 1 == (size_t)r) {
            break;
        }
        offset += (size_t)r;
    }

    char message[TASK_LENGTH + sizeof(struct tm)];
    memcpy(message, task, TASK_LENGTH);
    memcpy(message + TASK_LENGTH, task_date, sizeof(struct tm));

    if(rpc_send_buf(AT_RPC_SOCKET, ADD_JOB_MESSAGE, message, TASK_LENGTH + sizeof(struct tm), 5 * 1000 * 1000)) {
        P_ERR("failed to send message: %s", message);
        return EXIT_FAILURE;
    } else {
        P_INFO("SENT MESSAGE: %s", message);
    }

    return EXIT_SUCCESS;
}

int send_remove_task_rpc(int job_number)
{
    P_INFO_STR("about to send remove");

    if(rpc_send_buf(AT_RPC_SOCKET, REMOVE_JOB_MESSAGE, &job_number, sizeof(int), 5 * 1000 * 1000)) {
        P_ERR("failed to send remove message for job number: %d", job_number);
        return EXIT_FAILURE;
    } else {
        P_INFO("sent remove job number %d message", job_number);
    }

    return EXIT_SUCCESS;
}

int send_clear_sll_rpc(void)
{
    P_INFO_STR("about to send clear sll");
    if(rpc_trigger_action(AT_RPC_SOCKET, CLEAR_SLL_MESSAGE, 5 * 10000 * 1000)) {
        P_ERR_STR("failed to send clear sll");
        return EXIT_FAILURE;
    } else {
        P_INFO_STR("sent clear sll");
    }
    return EXIT_SUCCESS;
}

int send_close_db_rpc(void)
{
    P_INFO_STR("about to send close");
    if(rpc_trigger_action(AT_RPC_SOCKET, CLOSE_DB_MESSAGE, 5 * 1000 * 1000)) {
        P_ERR_STR("failed to send close db");
        return EXIT_FAILURE;
    } else {
        P_INFO_STR("sent close db");
    }
    return EXIT_SUCCESS;
}

int send_open_and_create_db_rpc(void)
{
    P_INFO_STR("about to send open and create db");
    if(rpc_trigger_action(AT_RPC_SOCKET, OPEN_AND_CREATE_DB_MESSAGE, 5 * 1000 * 1000)) {
        P_ERR_STR("failed to send open and create db");
        return EXIT_FAILURE;
    } else {
        P_INFO_STR("sent open and create db");
    }
    return EXIT_SUCCESS;
}
