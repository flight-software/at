#ifndef AT_SLL_H
#define AT_SLL_H

#include <stdint.h>

#include "sqlite3.h"

/**
 * Callback function used to sort at_jobs in the SLL.
 *
 * @param a Pointer to the first job
 * @param b Pointer to the second job
 * @param arg Not used
 *
 * @return < 0 if first earlier than second
 *         = 0 if first same as second
 *         > 0 if first is later than second
 */
int sll_sort_job_cb(const void* a, const void* b, void* arg);

/**
 * Callback function for finding an at_job in the SLL.
 *
 * @param cur Pointer to the current job
 * @param looking_for Pointer to the integer of the desired job id
 *
 * @return 1 if cur is the desired job, 0 else
 */
uint8_t sll_search_job_cb(void* cur, void* looking_for);

/**
 * Clears and refills the SLL from the current sqlite3 database,
 *
 * @param db Pointer to the sqlite3 database
 * @param sorted_job_list Pointer to the SLL
 * @param sll_lock Mutex lock for SLL
 *
 * @return EXIT_SUCCESS else EXIT_FAILURE
 */
int recreate_sll(sqlite3* db, sll_fs* sorted_job_list, pthread_mutex_t* sll_lock);

#endif
